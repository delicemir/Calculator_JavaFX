package sample;

import javafx.scene.Node;
import javafx.scene.control.Label;

public class MyLabelSignature extends Label {

    public MyLabelSignature() {

        setPrefWidth(150);
        setPrefHeight(25);

    }

    public MyLabelSignature(String text) {
        super(text);
    }

    public MyLabelSignature(String text, Node graphic) {
        super(text, graphic);
    }
}
