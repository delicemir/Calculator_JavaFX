package sample;

import javafx.scene.Node;
import javafx.scene.control.Label;

public class MyLabelResult extends Label {

    public MyLabelResult() {

        setPrefWidth(200);
        setPrefHeight(50);

    }

    public MyLabelResult(String text) {
        super(text);
    }

    public MyLabelResult(String text, Node graphic) {
        super(text, graphic);
    }
}
