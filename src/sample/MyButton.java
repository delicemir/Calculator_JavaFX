package sample;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Font;

public class MyButton extends Button {



    public MyButton() {
        setPrefHeight(50);
        setPrefWidth(50);

    }

    public MyButton(String text) {
        super(text);
    }

    public MyButton(String text, Node graphic) {
        super(text, graphic);
    }
}
