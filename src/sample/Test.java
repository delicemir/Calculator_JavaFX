package sample;

import java.sql.*;

public class Test {

    public String lastCh(String test) {
        return test.substring(test.length() - 1, test.length());
    }

    public boolean checkLastChar(String test) {

        if(!lastCh(test).contains("+")||!lastCh(test).contains("-")||!lastCh(test).contains("*")||!lastCh(test).contains("/")) {
            return true;
        }
        return false;

    }

    public static void main(String[] args) {
        /*String test = "Test+data+";
        char[] operands= {'+','-','*','/'};

        System.out.println(test.substring(test.length()-1,test.length()));

        for (char operand: operands) {
            if(test.contains(String.valueOf(operand))) {
                System.out.println("Sadrži operande!");
            }
        }

        int k = 0;
        for (int i = 0; i <test.length() ; i++) {
            if(test.charAt(i)=='+') {
                k++;
            }
        }
        System.out.println(k);*/

        String mathText="2+power(2,3)+(2-7+9)+sqrt(7)";

        try {
            Class. forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite::memory:");
            String sql= "SELECT "+mathText;
            PreparedStatement ptst =conn.prepareStatement(sql);
            ResultSet rs = ptst.executeQuery();
            rs.next();
            System.out.println("Result of math. expression: ["+mathText + "] is: "+rs.getDouble(1));
            ptst.close();
            conn.close();
        } catch (ClassNotFoundException e) {
            System.out.println("Error ClassNotFoundException: "+e);
        } catch (SQLException e) {
            System.out.println("Error SQLException: "+e);
        }


    }

}
