package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public Pane backgroundPane;
    public MyLabelResult result;

    public MyButton btn9;
    public MyButton btn8;
    public MyButton btn7;
    public MyButton btn6;
    public MyButton btn5;
    public MyButton btn4;
    public MyButton btn3;
    public MyButton btn2;
    public MyButton btn1;
    public MyButton btn0;

    public MyButton btnPlus;
    public MyButton btnMin;
    public MyButton btnMul;
    public MyButton btnDiv;

    public MyButton btnPlusMinus;
    public MyButton btnDot;
    public MyButton btnLeftBracket;
    public MyButton btnC;
    public MyButton btnCE;
    public MyButton btnCalculate;
    public MyButton btnRightBracket;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        result.setText("");

        btn1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "1");
            }
        });

        btn2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "2");
            }
        });

        btn3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "3");
            }
        });
        btn4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "4");
            }
        });
        btn5.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "5");
            }
        });
        btn6.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "6");
            }
        });
        btn7.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "7");
            }
        });
        btn8.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "8");
            }
        });
        btn9.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "9");
            }
        });
        btn0.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "0");
            }
        });

        btnDot.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (result.getText().length() != 0 &&!checkLastChar(result.getText())) {
                    result.setText(result.getText() + ".");
                }
            }

        });

        btnPlus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(result.getText().length()!=0&&!checkLastChar(result.getText())) {
                    result.setText(result.getText() + "+");
                }

            }
        });

        btnMin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(result.getText().length()!=0&&!checkLastChar(result.getText())) {
                    result.setText(result.getText() + "-");
                }
            }
        });
        btnMul.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(result.getText().length()!=0&&!checkLastChar(result.getText())) {
                    result.setText(result.getText() + "*");
                }
            }
        });
        btnDiv.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(result.getText().length()!=0&&!checkLastChar(result.getText())) {
                    result.setText(result.getText() + "/");
                }
            }
        });

        btnPlusMinus.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(result.getText().length()!=0&&!checkString(result.getText())) {
                    if (Double.parseDouble(result.getText()) > 0) {
                        result.setText("-" + result.getText());
                    }
                    else if(Double.parseDouble(result.getText()) < 0) {
                        result.setText(result.getText().substring(1,result.getText().length()));
                    }
                }
            }
        });

        btnCE.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText("");
            }
        });

        btnC.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(result.getText().length()!=0) {
                    result.setText(result.getText().substring(0,result.getText().length()-1));
                }

            }
        });

        btnLeftBracket.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + "(");
            }
        });

        btnRightBracket.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                result.setText(result.getText() + ")");
            }
        });


        btnCalculate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                try {
                    if(result.getText().length()!=0) {
                        if ((countLeftBrackets(result.getText()) == countRightBrackets(result.getText()))&&!checkLastChar(result.getText())) {

                            ScriptEngineManager sem = new ScriptEngineManager();
                            ScriptEngine se = sem.getEngineByName("JavaScript");
                            result.setText(String.valueOf(se.eval(result.getText())));

                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error: " + e);
                }

            }
        });


    }


    // POMOĆNE METODE

    public String lastChar(String test) {
        return test.substring(test.length() - 1, test.length());
    }

    public boolean checkLastChar(String test) {

        if (lastChar(test).contains("+") || lastChar(test).contains("-") || lastChar(test).contains("*") || lastChar(test).contains("/")|| lastChar(test).contains(".")) {
            return true;
        }

        return false;
    }

    public boolean checkString(String test) {
        String temp="";
        if(test.length()!=0) {
            temp = test.substring(1,test.length());
        }
        if ((temp).contains("+") || (temp).contains("-") || (temp).contains("*") || (temp).contains("/")) {
            return true;
        }

        return false;

    }


    public int countLeftBrackets(String test) {
        int k=0;
        for (int i = 0; i <test.length() ; i++) {
            if(test.charAt(i)=='(') {
                k++;
            }
        }

        return k;
    }

    public int countRightBrackets(String test) {
        int k=0;
        for (int i = 0; i <test.length() ; i++) {
            if(test.charAt(i)==')') {
                k++;
            }
        }

        return k;
    }


}

